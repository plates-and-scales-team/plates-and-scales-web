import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MDBBootstrapModule } from 'angular-bootstrap-md';

@NgModule({
  declarations: [],
  exports: [
    CommonModule,
  ]
})
export class SharedModule { }
