import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeBreakSecondComponent } from './home-break-second.component';

describe('HomeBreakSecondComponent', () => {
  let component: HomeBreakSecondComponent;
  let fixture: ComponentFixture<HomeBreakSecondComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeBreakSecondComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeBreakSecondComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
