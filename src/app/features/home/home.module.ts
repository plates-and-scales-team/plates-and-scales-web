import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { HomeIntroComponent } from './home-intro/home-intro.component';
import { HomeServicesComponent } from './home-services/home-services.component';
import { HomeBreakFirstComponent } from './home-break-first/home-break-first.component';
import { HomeBreakSecondComponent } from './home-break-second/home-break-second.component';
import { HomeAboutUsComponent } from './home-about-us/home-about-us.component';
import { HomeContactUsComponent } from './home-contact-us/home-contact-us.component';
import { HomeBookingsComponent } from './home-bookings/home-bookings.component';

@NgModule({
  declarations: [
    HomeComponent,
    HomeIntroComponent,
    HomeServicesComponent,
    HomeBreakFirstComponent,
    HomeBreakSecondComponent,
    HomeAboutUsComponent,
    HomeContactUsComponent,
    HomeBookingsComponent
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    HomeComponent,
  ]
})
export class HomeModule { }
