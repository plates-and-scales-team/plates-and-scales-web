import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeBreakFirstComponent } from './home-break-first.component';

describe('HomeBreakFirstComponent', () => {
  let component: HomeBreakFirstComponent;
  let fixture: ComponentFixture<HomeBreakFirstComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeBreakFirstComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeBreakFirstComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
