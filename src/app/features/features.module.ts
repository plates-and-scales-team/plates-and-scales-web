import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { HomeModule } from './home/home.module';

@NgModule({
  declarations: [],
  imports: [
    SharedModule,
  ],
  exports: [
    HomeModule,
  ]
})
export class FeaturesModule { }
