import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { NavModule } from './nav/nav.module';

@NgModule({
  declarations: [],
  imports: [
    SharedModule,
  ],
  exports: [
    NavModule
  ]
})
export class PublicModule { }
