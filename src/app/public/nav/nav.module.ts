import { NgModule } from '@angular/core';
import { NavbarModule, WavesModule } from 'angular-bootstrap-md'
import { NavComponent } from './nav.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [
    NavComponent
  ],
  imports: [
    NavbarModule,
    SharedModule,
  ],
  exports: [
    NavComponent,
  ]
})
export class NavModule { }
