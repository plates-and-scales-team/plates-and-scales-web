# base image
FROM node:10-alpine as builder

# set working directory
RUN mkdir /usr/src && mkdir /usr/src/app
WORKDIR /usr/src/app

# copy app into docker
COPY . /usr/src/app

# install dependencies 
RUN npm install

# generate build
RUN npm run build

# base image
FROM nginx:1.13.9-alpine

# copy artifact build from the 'build environment'
COPY --from=builder /usr/src/app/dist /usr/share/nginx/html

# expose port 80
EXPOSE 80

# run nginx
CMD ["nginx", "-g", "daemon off;"]

# docker build .
# docker images
# docker run -it image_name sh
# docker run -it -p 80:80 image_name
